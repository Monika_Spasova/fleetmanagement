const baseURL = 'http://localhost:3000';
// const baseURL = 'https://fleetmanagement-d4c6c.firebaseapp.com';
const apiURL = 'http://localhost:19631';
// const apiURL = 'https://demo-fleetmanagement.azurewebsites.net/';

export { baseURL, apiURL };