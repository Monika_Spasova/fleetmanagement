﻿namespace Infrastructure.Helpers
{
    using System.Net;
    using System.Net.Mail;
    using System.Threading.Tasks;

    public class MailHelper
    {
        public static async Task SendEmail(string recipients, string subject, string body)
        {
            var userName = "azure_0f6fccba399add2218d95963faf2a69d@azure.com";
            var password = "FleetManagementMoni69";

            var credentials = new NetworkCredential(userName, password);
            var client = new SmtpClient("smtp.sendgrid.net", 587)
                             {
                                 Credentials = credentials,
                                 EnableSsl = true,
                             };

            var mailMessage = new MailMessage("fleet.management.official@gmail.com", recipients, subject, body);
            await client.SendMailAsync(mailMessage);
        }
    }
}